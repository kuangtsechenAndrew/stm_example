/*
 * main implementation: use this 'C' sample to create your own application
 *
 */
/*******************************************************************************
 * NXP Semiconductor Inc.
 * (c) Copyright 2015 NXP Semiconductor, Inc.
 * ALL RIGHTS RESERVED.
 ********************************************************************************
 Services performed by NXP in this matter are performed AS IS and without
 any warranty. CUSTOMER retains the final decision relative to the total design
 and functionality of the end product. NXP neither guarantees nor will be
 held liable by CUSTOMER for the success of this project.
 NXP DISCLAIMS ALL WARRANTIES, EXPRESSED, IMPLIED OR STATUTORY INCLUDING,
 BUT NOT LIMITED TO, IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR
 A PARTICULAR PURPOSE ON ANY HARDWARE, SOFTWARE ORE ADVISE SUPPLIED
 TO THE PROJECT BY NXP, AND OR NAY PRODUCT RESULTING FROM NXP
 SERVICES. IN NO EVENT SHALL NXP BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL
 DAMAGES ARISING OUT OF THIS AGREEMENT.
 CUSTOMER agrees to hold NXP harmless against any and all claims demands
 or actions by anyone on account of any damage, or injury, whether commercial,
 contractual, or tortuous, rising directly or indirectly as a result
 of the advise or assistance supplied CUSTOMER in connection with product,
 services or goods supplied under this Agreement.
 ********************************************************************************
 * File:             MPC5744P-ETimerCountMode-S32DS.c
 * Owner:            Martin Kovar
 * Version:          1.0
 * Date:             Jan-12-2016
 * Classification:   General Business Information
 * Brief:            ETimer count mode demonstration
 ********************************************************************************
 * Detailed Description:
 * Application performs basic initialization, setup PLL1 to maximum allowed freq. PLL1 is system frequency,
 * PLL0 in initialized to 50MHz
 * initializes peripherals clock (MOTC_CLK is set to 5MHz)
 * initializes eTimer to count mode
 * initializes interrupts, blinking one LED by eTimer interrupt,
 *
 * ------------------------------------------------------------------------------
 * Test HW:         XDEVKIT-MPC5744P
 * MCU:             PPC5744PFMMM8 1N65H
 * Fsys:            200 MHz
 * Debugger:        OpenSDA
 * Target:          FLASH
 * EVB connection:  none
 *
 * ------------------------------------------------------------------------------
 *
 ********************************************************************************
 Revision History:
 Version  Date            Author  			Description of Changes
 1.0      Dec-29-2015     Martin Kovar  		Initial Version
 *******************************************************************************/
#include "derivative.h" /* include peripheral declarations */
#include "project.h"


/*******************************************************************************
 * Constants and macros
 *******************************************************************************/

uint16_t g_counter = 0;

/*******************************************************************************
 * Function prototypes
 *******************************************************************************/
static void SysClk_Init(void);
static void InitPeriClkGen(void);
static void HW_Init(void);

extern void xcptn_xmpl(void);

/*******************************************************************************
 * Local functions
 *******************************************************************************/
void LED_Config(void);
void led_toggle(void);
/*******************************************************************************
 Function Name : HW_init
 Engineer      : Martin Kovar
 Date          : Dec-29-2015
 Parameters    :
 Modifies      :
 Returns       :
 Notes         : initialization of the hw for the purposes of this example
 Issues        :
 *******************************************************************************/
static void HW_Init(void)
{
    xcptn_xmpl( ); /* Configure and Enable Interrupts */
    SysClk_Init( );
    InitPeriClkGen( );

    LED_Config( );
}

/*******************************************************************************
 Function Name : Sys_Init
 Engineer      : Martin Kovar
 Date          : Dec-29-2015
 Parameters    : NONE
 Modifies      : NONE
 Returns       : NONE
 Notes         : Enable XOSC, PLL0, PLL1 and enter RUN0 with PLL1 as sys clk (200 MHz)
 Issues        : NONE
 *******************************************************************************/
// Enable XOSC, PLL0, PLL1 and enter RUN0 with PLL1 as sys clk (200 MHz)
void SysClk_Init(void)
{
    MC_CGM.AC3_SC.B.SELCTL = 0x01;		    //connect XOSC to the PLL0 input
    MC_CGM.AC4_SC.B.SELCTL = 0x01;		    //connect XOSC to the PLL1 input

    // Set PLL0 to 50 MHz with 40MHz XOSC reference
    PLLDIG.PLL0DV.R = 0x3008100A;// PREDIV =  1, MFD = 10, RFDPHI = 8, RFDPHI1 = 6

    MC_ME.RUN0_MC.R = 0x00130070;	// RUN0 cfg: IRCON,OSC0ON,PLL0ON,syclk=IRC

    // Mode Transition to enter RUN0 mode:
    MC_ME.MCTL.R = 0x40005AF0;		    // Enter RUN0 Mode & Key
    MC_ME.MCTL.R = 0x4000A50F;		    // Enter RUN0 Mode & Inverted Key
    while (MC_ME.GS.B.S_MTRANS)
    {
    };		    // Wait for mode transition to complete
    while (MC_ME.GS.B.S_CURRENT_MODE != 4)
    {
    };	    // Verify RUN0 is the current mode

    // Set PLL1 to 200 MHz with 40MHz XOSC reference
    PLLDIG.PLL1DV.R = 0x00020014;	     // MFD = 20, RFDPHI = 2

    MC_ME.RUN_PC[0].R = 0x000000FE;		  // enable peripherals run in all modes
    MC_ME.RUN0_MC.R = 0x001300F4;// RUN0 cfg: IRCON, OSC0ON, PLL1ON, syclk=PLL1

    MC_CGM.SC_DC0.R = 0x80030000; // PBRIDGE0/PBRIDGE1_CLK at syst clk div by 4 ... (50 MHz)

    // Mode Transition to enter RUN0 mode:
    MC_ME.MCTL.R = 0x40005AF0;		    // Enter RUN0 Mode & Key
    MC_ME.MCTL.R = 0x4000A50F;		    // Enter RUN0 Mode & Inverted Key
    while (MC_ME.GS.B.S_MTRANS)
    {
    };		    // Wait for mode transition to complete
    while (MC_ME.GS.B.S_CURRENT_MODE != 4)
    {
    };	    // Verify RUN0 is the current mode

}

/*******************************************************************************
 Function Name : PeriClkGen_init
 Engineer      : Martin Kovar
 Date          : Jan-5-2016
 Parameters    :
 Modifies      :
 Returns       :
 Notes         : - Enable all auxiliary clocks, IMPORTANT - MOTC_CLK is set to 5MHz
 Issues        :
 *******************************************************************************/
void InitPeriClkGen(void)
{
    // MC_CGM.SC_DC0.R = 0x80030000;    // PBRIDGE0/PBRIDGE1_CLK at syst clk div by 4 ... (50 MHz)
    MC_CGM.AC0_SC.R = 0x02000000;    // Select PLL0 for auxiliary clock 0
    MC_CGM.AC0_DC0.R = 0x80090000; // MOTC_CLK : Enable aux clk 0 div by 10 嚙� (5 MHz)
    MC_CGM.AC0_DC1.R = 0x80070000; // SGEN_CLK : Enable aux clk 0 div by 8 嚙� (6.25 MHz)
    MC_CGM.AC0_DC2.R = 0x80010000; // ADC_CLK : Enable aux clk 0 div by 2 嚙� (25 MHz)
    MC_CGM.AC6_SC.R = 0x04000000;    // Select PLL1 for auxiliary clock 6
    MC_CGM.AC6_DC0.R = 0x80010000; // CLKOUT0 : Enable aux clk 6 div by 2 嚙� (100 MHz)
    MC_CGM.AC10_SC.R = 0x04000000;    // Select PLL1 for auxiliary clock 10
    MC_CGM.AC10_DC0.R = 0x80030000; // ENET_CLK : Enable aux clk 10 div by 4 嚙� (50 MHz)
    MC_CGM.AC11_SC.R = 0x04000000;    // Select PLL1 for auxiliary clock 11
    MC_CGM.AC11_DC0.R = 0x80030000; // ENET_TIME_CLK : Enable aux clk 11 div by 4 嚙� (50 MHz)
    MC_CGM.AC5_SC.R = 0x02000000;    // Select PLL0 for auxiliary clock 5
    MC_CGM.AC5_DC0.R = 0x80000000; // LFAST_CLK : Enable aux clk 5 div by 1 嚙� (50 MHz)
    MC_CGM.AC2_DC0.R = 0x80010000; // CAN_PLL_CLK : Enable aux clk 2 (PLL0) div by 2 嚙� (25 MHz)
    MC_CGM.AC1_DC0.R = 0x80010000; // FRAY_PLL_CLK : Enable aux clk 1 (PLL0) div by 2 嚙� (25 MHz)
    MC_CGM.AC1_DC1.R = 0x80010000; // SENT_CLK : Enable aux clk 1 (PLL0) div by 2 嚙� (25 MHz)
}

void led_toggle_1(void)
{
    static uint8_t state = 0;

    if (state == 0)
    {
        state = 1;
        SIUL2.GPDO[PC11].R =  0;

    }
    else
    {
        state = 0;
        /* Turn off LEDs. LEDs are active low */
        SIUL2.GPDO[PC11].R = 1;

    }
}

void led_toggle_2(void)
{
    static uint8_t state = 0;

    if (state == 0)
    {
        state = 1;
        SIUL2.GPDO[PC12].R = 0;
    }
    else
    {
        state = 0;
        /* Turn off LEDs. LEDs are active low */
        SIUL2.GPDO[PC12].R = 1;
    }
}

void led_toggle_3(void)
{
    static uint8_t state = 0;

    if (state == 0)
    {
        state = 1;
        SIUL2.GPDO[PC13].R = 0;
    }
    else
    {
        state = 0;
        /* Turn off LEDs. LEDs are active low */
        SIUL2.GPDO[PC13].R = 1;
    }
}

void LED_Config(void)
{
    /* Assign LED ports as GPIO outputs */

    /* Configure the RGB LED on DEVKIT-MPC5744P */
    SIUL2.MSCR[PC11].B.OBE = 1;
    SIUL2.MSCR[PC12].B.OBE = 1;
    SIUL2.MSCR[PC13].B.OBE = 1;

    /* Turn off LEDs. LEDs are active low */
    SIUL2.GPDO[PC11].R = 1;
    SIUL2.GPDO[PC12].R = 1;
    SIUL2.GPDO[PC13].R = 1;
}

__attribute__ ((section(".text")))
int main(void)
{
    int counter = 0;
    uint8_t NumberOfEven = 0;

    HW_Init( );

    stm_init( );
    stm_event_register(led_toggle_1, 1000);
    stm_event_register(led_toggle_2, 1000);
    stm_event_register(led_toggle_3, 1000);

    stm_event_enable(led_toggle_1, TRUE);
    stm_event_enable(led_toggle_2, TRUE);
    stm_event_enable(led_toggle_3, TRUE);

    NumberOfEven = get_available_timer_event();

    INTC_0.PSR[611].R = 0x8001;    //set interrupt priority
    INTC_0.PSR[36].R = 0x8002;     //set interrupt priority


    /* Loop forever */
    while (1)
    {
        counter++;
    }

    return 0;
}
