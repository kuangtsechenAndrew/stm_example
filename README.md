# MPC5744P STM(System Timer Module)

This project will show you how to access STM of MPC5744P.
Futhermore, we will using STM to create a software timer function.
Using NON-preemptive and first-come-first-serve.
Please note that this project is inherited from the built-in example of S32D.

#### ```/src``` folder:  
This folder contains the firmware and main.c files.
The adc.c file originally does NOT exist in the original official example.

#### ```/include``` folder:
This folder contains some necessary header files. adc.h and project.h does not exist in the original example.

#### To do list :
Add priority for the software timer.
