/*
 * timer.h
 *
 *  Created on: 2021�~1��8��
 *      Author: andrew
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "MPC5744P.h"
#include "typedefs.h"

typedef void (*callback_void_t)(void);
typedef struct TIMER_EVENT_STR
{
    uint8_t EventArryIndex;
    callback_void_t CallbackFunct;
    uint32_t Period;
    bool EnableState;
} timer_event;

/* STM function prototype */
uint8_t get_available_timer_event();
void stm_init(void);
void stm_isr(void);
bool stm_event_register(callback_void_t, uint32_t);
bool stm_event_enable(callback_void_t , bool );

/*Time related function prototype*/
uint32_t get_milli_second(void);
uint32_t get_second(void);

#endif /* TIMER_H_ */
